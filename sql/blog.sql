SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `blog` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `blog` ;

-- -----------------------------------------------------
-- Table `blog`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blog`.`user` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(256) NOT NULL,
  `mail` VARCHAR(256) NULL,
  `password` VARCHAR(45) NULL,
  `type` VARCHAR(32) NOT NULL DEFAULT 'comment',
  `phone` VARCHAR(45) NULL,
  `qq` VARCHAR(45) NULL,
  `sex` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
COMMENT = '用户表';


-- -----------------------------------------------------
-- Table `blog`.`category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blog`.`category` (
  `id` BIGINT(11) NOT NULL,
  `name` VARCHAR(256) NOT NULL COMMENT '分类名	',
  `description` VARCHAR(512) NULL COMMENT '描述',
  `sort` INT NULL,
  `createtime` DATETIME NULL COMMENT '创建时间',
  `edit_userid` BIGINT(10) NULL COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `edit_userid_UNIQUE` (`edit_userid` ASC),
  CONSTRAINT `fk_category_1`
    FOREIGN KEY (`edit_userid`)
    REFERENCES `blog`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '分类导航';


-- -----------------------------------------------------
-- Table `blog`.`article`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blog`.`article` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `categoryid` BIGINT(11) NULL COMMENT '分类',
  `title` VARCHAR(256) NOT NULL COMMENT '标题',
  `content` TEXT NULL COMMENT '内容',
  `count_view` BIGINT(11) NULL DEFAULT 0 COMMENT '浏览统计',
  `count_comment` BIGINT(11) NULL DEFAULT 0 COMMENT '评论统计',
  `type` VARCHAR(32) NULL COMMENT '类型',
  `status` VARCHAR(32) NULL COMMENT '状态-显示-隐藏',
  `sort` INT NULL DEFAULT 0 COMMENT '排序',
  `edit_userid` BIGINT(11) NULL COMMENT '编辑人',
  `createtime` DATETIME NULL COMMENT '创建时间',
  `img_url` VARCHAR(45) NULL COMMENT '图片连接',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `categoryid_UNIQUE` (`categoryid` ASC),
  UNIQUE INDEX `edit_userid_UNIQUE` (`edit_userid` ASC),
  CONSTRAINT `fk_article_1`
    FOREIGN KEY (`categoryid`)
    REFERENCES `blog`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_article_2`
    FOREIGN KEY (`edit_userid`)
    REFERENCES `blog`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '文章';


-- -----------------------------------------------------
-- Table `blog`.`comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `blog`.`comment` (
  `id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` BIGINT(11) NULL,
  `user_id` BIGINT(11) NULL COMMENT '评论者',
  `comment` TEXT NULL COMMENT '评论内容',
  `status` VARCHAR(32) NULL DEFAULT 'show' COMMENT '是否予以显示',
  `createtime` DATETIME NULL COMMENT '评论时间',
  `article_id` BIGINT(11) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `parent_id_UNIQUE` (`parent_id` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  UNIQUE INDEX `article_id_UNIQUE` (`article_id` ASC),
  CONSTRAINT `fk_comment_1`
    FOREIGN KEY (`parent_id`)
    REFERENCES `blog`.`comment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_2`
    FOREIGN KEY (`article_id`)
    REFERENCES `blog`.`article` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_3`
    FOREIGN KEY (`user_id`)
    REFERENCES `blog`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '评论';


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
