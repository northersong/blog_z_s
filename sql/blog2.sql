/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50623
Source Host           : localhost:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2015-08-10 10:46:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `categoryid` bigint(11) DEFAULT NULL COMMENT '分类',
  `title` varchar(256) NOT NULL COMMENT '标题',
  `summary` text COMMENT '摘要',
  `content` text COMMENT '内容',
  `count_view` bigint(11) DEFAULT '0' COMMENT '浏览统计',
  `count_comment` bigint(11) DEFAULT '0' COMMENT '评论统计',
  `type` varchar(32) DEFAULT NULL COMMENT '类型',
  `status` varchar(32) DEFAULT NULL COMMENT '状态-显示-隐藏',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `edit_userid` bigint(11) DEFAULT NULL COMMENT '编辑人',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `img_url` varchar(45) DEFAULT NULL COMMENT '图片连接',
  PRIMARY KEY (`id`),
  KEY `categoryid_UNIQUE` (`categoryid`) USING BTREE,
  KEY `edit_userid_UNIQUE` (`edit_userid`) USING BTREE,
  CONSTRAINT `fk_article_1` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_article_2` FOREIGN KEY (`edit_userid`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='文章';

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '1', 'test1', 'summary is short ', 'sssszxcccccccccsdfsdfsdfds', '0', '0', 'blog', 'show', '4', null, '2015-08-03 11:05:46', null);
INSERT INTO `article` VALUES ('2', '2', '23', '23', '222', '0', '2', 'blog', 'show', '1', null, '2015-08-06 15:00:43', null);
INSERT INTO `article` VALUES ('3', '1', '2', '222', '222', '0', '0', 'blog', 'show', '2', null, '2015-08-06 15:01:05', null);
INSERT INTO `article` VALUES ('4', '2', '2', '222', '222', '0', '0', 'blog', 'show', '3', null, '2015-08-06 15:03:16', null);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` bigint(11) NOT NULL,
  `name` varchar(256) NOT NULL COMMENT '分类名	',
  `description` varchar(512) DEFAULT NULL COMMENT '描述',
  `sort` int(11) DEFAULT NULL,
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `edit_userid` bigint(10) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `edit_userid_UNIQUE` (`edit_userid`) USING BTREE,
  CONSTRAINT `fk_category_1` FOREIGN KEY (`edit_userid`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类导航';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '我的博客', null, null, null, null);
INSERT INTO `category` VALUES ('2', '点滴积累', null, null, null, null);

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(11) DEFAULT NULL,
  `subject` varchar(1024) DEFAULT NULL,
  `user_id` bigint(11) DEFAULT NULL COMMENT '评论者',
  `comment` text COMMENT '评论内容',
  `status` varchar(32) DEFAULT 'show' COMMENT '是否予以显示',
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论时间',
  `article_id` bigint(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `parent_id_UNIQUE` (`parent_id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  UNIQUE KEY `article_id_UNIQUE` (`article_id`),
  CONSTRAINT `fk_comment_1` FOREIGN KEY (`parent_id`) REFERENCES `comment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_2` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comment_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='评论';

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('1', null, null, '1', '11', 'show', '2015-08-06 11:36:10', null);
INSERT INTO `comment` VALUES ('2', null, '1111', '3', '111', 'show', '2015-08-06 13:57:38', null);
INSERT INTO `comment` VALUES ('3', null, '123', '4', '3333', 'show', '2015-08-06 14:00:52', null);
INSERT INTO `comment` VALUES ('4', null, '222', '5', '222222', 'show', '2015-08-06 14:09:11', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'comment',
  `phone` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qq` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'song', null, null, 'comment', null, null, null, '2015-08-06 11:37:38');
INSERT INTO `user` VALUES ('2', '1', '22@a', null, 'comment', null, null, null, '2015-08-06 12:00:46');
INSERT INTO `user` VALUES ('3', '1', '11#@q.c', null, 'comment', null, null, null, '2015-08-06 13:57:38');
INSERT INTO `user` VALUES ('4', '22', '222@q.cs', null, 'comment', null, null, null, '2015-08-06 14:00:52');
INSERT INTO `user` VALUES ('5', '123', '123@q.c', null, 'comment', null, null, null, '2015-08-06 14:09:11');
