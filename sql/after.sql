ALTER TABLE `blog`.`article` 
ADD COLUMN `summary` TEXT NULL COMMENT '摘要' AFTER `title`;


ALTER TABLE `blog`.`article` 
ADD COLUMN `original_url` VARCHAR(256) NULL COMMENT '原文地址';
