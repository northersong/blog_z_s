package com.test;


import java.lang.reflect.Field;

import com.anna.config.Ioc.Autowired;
import com.anna.config.Ioc.Ioc;
import com.blog.service.ArticleManager;

public class Test {

	@Autowired
	private ArticleManager articleManager;
	
	public void print(){
		System.out.println("Hello Ioc");
	}
	
	public static void main(String...args){
		Ioc ioc = Ioc.getIoc();
		
		ioc.addPackage("com.blog.service",true);
		ioc.add("test",Test.class,true);
		Test test = (Test) Ioc.getBean("test");
		
		ArticleManager articleManager = (ArticleManager)Ioc.getBean(ArticleManager.class);
		
		Field[] fields = test.getClass().getDeclaredFields();
		for(Field f : fields){
			if(f!=null && f.isAnnotationPresent(Autowired.class)){
				Autowired autowired = f.getAnnotation(Autowired.class);
				Object value = null;
				if(autowired.value()!=null&&!"".equals(autowired.value())){
					value = Ioc.getBean(autowired.value());
				}else{
					value = Ioc.getBean(f.getType());
				}
				
				f.setAccessible(true);
				try {
					f.set(test, value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				
			}
		}
		
	//	articleManager.pri();
		
	//	test.articleManager.pri();
		
		test.print();
	}
}
