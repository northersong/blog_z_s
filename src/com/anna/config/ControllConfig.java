package com.anna.config;

import com.anna.config.Ioc.AutowiredInterceptor;
import com.anna.config.Ioc.Ioc;
import com.anna.config.handler.IpFilterHandler;
import com.anna.config.handler.UrlRewriterHandler;
import com.blog.controller.BlogController;
import com.blog.controller.CommentController;
import com.blog.controller.EditBlogController;
import com.blog.model.Article;
import com.blog.model.Category;
import com.blog.model.Comment;
import com.blog.model.User;
import com.controller.TulingRobotController;
import com.controller.WeixinController;
import com.controller.model.Reply;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.render.ViewType;

public class ControllConfig  extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		PropKit.use("a_little_config.txt");
		//me.setDevMode(PropKit.getBoolean("devMode", false));
		//开启JSP视图
		me.setDevMode(PropKit.getBoolean("devMode", false));
		me.setViewType(ViewType.JSP);
	}

	@Override
	public void configRoute(Routes me) {
		//访问路由配置
		me.add("/",  BlogController.class);
		me.add("/blog",  EditBlogController.class);
		me.add("/contact",  CommentController.class);
		me.add("/wx",WeixinController.class);
		me.add("/robot",TulingRobotController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		me.add(c3p0Plugin);
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin("blog" , c3p0Plugin);
		me.add(arp);
		arp.addMapping("user", User.class);	
		arp.addMapping("category",Category.class);
		arp.addMapping("article",Article.class);
		arp.addMapping("comment", Comment.class);
		arp.addMapping("reply", Reply.class);
		
		//12306数据源
/*		C3p0Plugin c3p12306 = new C3p0Plugin(PropKit.get("jdbcUrl12306"), PropKit.get("user"), PropKit.get("password").trim());
		me.add(c3p12306);
		ActiveRecordPlugin arp12306 = new ActiveRecordPlugin("12306", c3p12306);
		me.add(arp12306);
		arp12306.addMapping("v9_12306", User12306.class);*/
		
		Ioc ioc = Ioc.getIoc();
		me.add(ioc);
		ioc.addPackage("com.blog.service",true);
		//ioc.add("articleManager",ArticleManager.class,true);
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new AutowiredInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {
		me.add(new IpFilterHandler());
	 	me.add(new UrlRewriterHandler());
	}

}
