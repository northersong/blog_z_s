package com.anna.config.Interceptor;

import com.blog.model.User;
import com.blog.model.User.TYPEPer;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

public class AdminInterceptor implements Interceptor{

	public void intercept(Invocation inv) {
		
		Controller controller = inv.getController();
		
		User user = (User)controller.getSessionAttr("user_admin");
		
		if(user == null || !TYPEPer.isAdmin(user.getStr("type"))){
			
		}
		inv.invoke();
	}

}
