package com.anna.config.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;

public class IpFilterHandler extends Handler{

	private static Logger log = Logger.getLogger(Handler.class);
	
	@Override
	public void handle(String target, HttpServletRequest request,
			HttpServletResponse response, boolean[] isHandled) {
		
		String ip = IpHelp.getIpAddr(request);
		
		if(StrKit.isBlank(ip) || IpHelp.isBlackIp(ip)){
			log.error("被拦截的黑名单Ip ：" + ip);
			isHandled[0] = true;
			return ;
		}
		
		IpHelp.recordIp(ip);
		
		nextHandler.handle(target, request, response, isHandled);
		
	}
}
