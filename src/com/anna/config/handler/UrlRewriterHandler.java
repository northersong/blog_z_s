package com.anna.config.handler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

/**
 * 重写URL
 * @author song
 *
 */
public class UrlRewriterHandler extends Handler{

	private String rootPath = "/";
	private String[] rules = { "/(article)/([0-9]+).htm", "(/category)/([0-9]+).htm" };
	
	@Override
	public void handle(String target, HttpServletRequest request,
			HttpServletResponse response, boolean[] isHandled) {
		
		//只过滤 .htm后缀的链接
		if(target.endsWith(".htm")){
			
			Pattern p = null;
			Matcher m = null;
			
			for(String rule : rules){
				p =  Pattern.compile(rule);
				m= p.matcher(target);
				
				if(m.matches()){
					String id = m.group(2) ;
					request.setAttribute("id",id );
					target = rootPath + m.group(1) ;
					isHandled[0] = true;
					nextHandler.handle(target, request, response, isHandled);
					return ;
				}
			}
		}else if(target.indexOf(".") != -1){
			return ;
		}
		
		nextHandler.handle(target, request, response, isHandled);
		
	}

}
