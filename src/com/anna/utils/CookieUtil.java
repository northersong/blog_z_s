/**
 * cookie工具类
 */
package com.anna.utils;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.jfinal.kit.StrKit;

public class CookieUtil {

	public static Logger logger = Logger.getLogger(CookieUtil.class);
	/** 默认时间  1年*/
	public static int defaultAge = 365 * 24 * 60 * 60;
	/** 一天  **/
	public static int oneDay = 24 * 60 * 60;
	/**
	 * 写数据到cookies中
	 * 
	 * @param response
	 * @param name
	 * @param value
	 * @param seconds 以秒为单位
	 */
	public void setCookie(HttpServletResponse response, String key,String value, int seconds) {
		if(StrKit.isBlank(value))
		{
			return;
		}
		try 
		{
			Cookie cookie = new Cookie(key, URLEncoder.encode(value, "UTF-8"));
			cookie.setMaxAge(seconds);
			// cookie.setDomain(".192.117");
			cookie.setPath("/");
			response.addCookie(cookie);
		} catch (UnsupportedEncodingException e) {
			logger.error("写数据到cookies中出错!", e);
		}
	}
	
	/**
	 * 写数据到cookies中
	 * 
	 * @param response
	 * @param map{key, value, path, seconds}
	 * @param seconds
	 */
	public void setCookie(HttpServletResponse response, Map<String, Object> map) {
		String value = (String) map.get("value");
		if(StrKit.isBlank(value))
		{
			return;
		}
		try 
		{
			Cookie cookie = new Cookie((String)map.get("key"), URLEncoder.encode(value, "UTF-8"));
			cookie.setMaxAge(map.get("seconds") == null?Integer.MAX_VALUE:(Integer)map.get("seconds"));
			if (map.get("domain") != null) {
				cookie.setDomain((String)map.get("domain"));
			}
			cookie.setPath(map.get("path") == null?"/":(String)map.get("path"));
			response.addCookie(cookie);
		} catch (UnsupportedEncodingException e) {
			logger.error("写数据到cookies中出错!", e);
		}
	}

	/**
	 * 通过key得到cookies中的value
	 * 
	 * @param request
	 * @param key
	 * @return
	 */
	public String getString(HttpServletRequest request, String key) {
		String value = null;
		Cookie cookie = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals(key)) {
					cookie = cookies[i];
				}
			}

		}
		if (cookie != null)
			value = cookie.getValue();
		if (value == null)
			value = "";
		try {
			value = URLDecoder.decode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("通过key得到cookies中的value出错!", e);
		}
		return value;
	}
	/**
	 * 销毁cookie
	 * @param request
	 * @param response
	 */
	public void deletesCookie(HttpServletRequest request,  HttpServletResponse response) {  
		Cookie[] cookies = request.getCookies();  
		if (cookies != null) 
		{  
			for (int i = 0; i < cookies.length; i++) 
			{  
				Cookie cookie = cookies[i];  
				// 设置Cookie立即失效  
                cookie.setMaxAge(0);  
				cookie.setPath("/");
				response.addCookie(cookie);
			}  
		}
	}
	
	/**
	 * 根据名称, 域名销毁cookie
	 * @param request
	 * @param response
	 * @param name
	 */
	public void deletesCookieByName(HttpServletRequest request,  HttpServletResponse response, String key, String domain) {
		Cookie[] cookies = request.getCookies();  
		if (cookies != null) 
		{  
			for (int i = 0; i < cookies.length; i++) 
			{  
				Cookie cookie = cookies[i];
				if (cookie.getName().equals(key)) {
					// 设置Cookie立即失效  
	                cookie.setMaxAge(0);
	                cookie.setDomain(domain);
					cookie.setPath("/");
					response.addCookie(cookie);
				}
			}  
		}
	}
	
	/**
	 * 根据名称销毁cookie
	 * @param request
	 * @param response
	 * @param name
	 */
	public void deletesCookieByName(HttpServletRequest request,  HttpServletResponse response, String key) {
		Cookie[] cookies = request.getCookies();  
		if (cookies != null) 
		{  
			for (int i = 0; i < cookies.length; i++) 
			{  
				Cookie cookie = cookies[i];
				if (cookie.getName().equals(key)) {
					// 设置Cookie立即失效  
	                cookie.setMaxAge(0);  
					cookie.setPath("/");
					response.addCookie(cookie);
				}
			}  
		}
	}
	
	/**
	 * Cookies个数
	 * @param request
	 * @return
	 */
	public int getCookiesTotal(HttpServletRequest request){
		
		Cookie[] cookies = request.getCookies();//这样便可以获取一个cookie数组
		
		if(cookies == null || cookies.length == 0 ){
			return 1;
		}
		return cookies.length;
	}

	/**
	 * 验证是否存在 
	 * @param request
	 * @param key
	 * @return true存在，false不存在
	 */
	public boolean isExists(HttpServletRequest request, String key) {
		
		String value = null;
		Cookie cookie = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals(key)) {
					cookie = cookies[i];
				}
			}

		}
		if (cookie != null)
			value = cookie.getValue();
		if (value == null)
			value = "";
		try {
			value = URLDecoder.decode(value, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("通过key得到cookies中的value出错!", e);
		}
		return StrKit.notBlank(value);
	}
}
