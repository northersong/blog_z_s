package com.tulingRobot;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import com.controller.handle.MsgDefaultHandle;
import com.fastweixin.message.BaseMsg;
import com.fastweixin.message.req.TextReqMsg;
import com.fastweixin.util.NetWorkCenter;
import com.jfinal.kit.StrKit;

public class TulingRobotApi {

	final static String url = "http://www.tuling123.com/openapi/api";
	
	final static String key = "d083b27ebc09500fb46feba6b2244b40";
	final static String userId = "29330";
	
	public static String process(String content){
		
		if(StrKit.isBlank(content)){
			return null;
		}
		
		try {
			content = java.net.URLEncoder.encode(content,"utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}   
		
		Map<String,String> data = new HashMap<>();
		data.put("key", key);
		data.put("userid", userId);
		
		data.put("info", content);
		
		ResponseCallback back = new ResponseCallback();
		
		NetWorkCenter.get(url, data, back);
		
		if(back.getResultCode() == 200)
			return back.getResultJson();
		
		return null;
	}
	
	public static void main(String[] args) {
		MsgDefaultHandle handle = new MsgDefaultHandle();
		TextReqMsg message = new TextReqMsg("北京到石家庄火车 k1084");
		BaseMsg b = handle.handle(message);
		System.out.println(b);
	}
}

class ResponseCallback implements com.fastweixin.util.NetWorkCenter.ResponseCallback{

	private String resultJson = null;
	private int resultCode = 0;
	
	@Override
	public void onResponse(int resultCode, String resultJson) {
		this.resultJson = resultJson;
		this.resultCode = resultCode;
	}
	
	public String getResultJson() {
		return resultJson;
	}
	public int getResultCode() {
		return resultCode;
	}

}
