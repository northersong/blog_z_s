package com.tulingRobot.msg;

import static com.anna.utils.json.Utils.jsonUtil;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.fastweixin.message.Article;
import com.fastweixin.message.NewsMsg;

public class Train {

	private String trainnum; //车次
	/** 起始站*/
	private String start; 
	
	private String starttime;
	/**到达站*/
	private String terminal;
	private String endtime;
	private String detailurl;
	private String icon;
	
	
	
	public static void toMsg(NewsMsg msg,String str,String query) throws JsonParseException, JsonMappingException, IOException{
		
		Article article = null;
		Train train;
		StringBuffer sb = null;
		List<Object> list = jsonUtil.json2ObjectList(str, Train.class);
		
		int listSize = list.size();
		
		if(listSize > msg.getMaxSize()){
			List<Object> overflow = list.subList(9, listSize);
			
			StringBuffer description = new StringBuffer();
			
			for(Object obj : overflow){
				train = (Train) obj;
				description.append(train.getStart() + ">" + train.getTerminal() + ":").append(train.getTrainnum() + " ").append(train.getStarttime() + "-" +train.getEndtime() + "\t");
			}
			msg.add("列车::" + description.toString(),"http://宋扬.top/robot?info=" + query);
			
			list = list.subList(0, 9);
		}
		
		for(Object obj : list){
			train = (Train) obj;
			sb = new StringBuffer();
			sb.append(train.getStart() + "->" + train.getTerminal()).append(
					"\t 时间：" + train.getStarttime() + "-" + train.getEndtime());
			article = new Article(train.getTrainnum() + "  " + sb.toString());
			
			article.setPicUrl(train.getIcon());
			msg.add(article);
		}
		
	}
	
	
	
	
	public String getTrainnum() {
		return trainnum;
	}
	public void setTrainnum(String trainnum) {
		this.trainnum = trainnum;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getTerminal() {
		return terminal;
	}
	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getDetailurl() {
		return detailurl;
	}
	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	

}
