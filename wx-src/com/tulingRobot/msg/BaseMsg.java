package com.tulingRobot.msg;

import java.util.List;
import java.util.Map;

public class BaseMsg {

	private String code;
	
	private String text;
	
	private List<Map<String,String>> list;
	
	
	public List<Map<String,String>> getList() {
		return list;
	}

	public void setList(List<Map<String,String>> list) {
		this.list = list;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	
}
