package com.tulingRobot.msg;

import static com.anna.utils.json.Utils.jsonUtil;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.fastweixin.message.Article;
import com.fastweixin.message.NewsMsg;

public class News {

	private String article;
	private String source;
	private String detailurl;
	private String icon;
	
	
	public static void toMsg(NewsMsg msg,String str) throws JsonParseException, JsonMappingException, IOException{
		
		Article article = null;
		News news;
		List<Object> list = jsonUtil.json2ObjectList(str, News.class);
		
		if(list.size() > msg.getMaxSize()){
			list = list.subList(0, 9);
		}
		
		for(Object obj : list){
			news = (News) obj;
			article = new Article(news.getArticle());
			article.setDescription("来自：" + news.getSource());
			article.setPicUrl(news.getIcon());
			article.setUrl(news.getDetailurl());
			msg.add(article);
		}
	}
	
	
	
	public String getArticle() {
		return article;
	}
	public void setArticle(String article) {
		this.article = article;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDetailurl() {
		return detailurl;
	}
	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	
	
}
