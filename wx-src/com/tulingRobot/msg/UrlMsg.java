package com.tulingRobot.msg;

public class UrlMsg extends BaseMsg {

	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
