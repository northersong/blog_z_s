package com.controller.handle;

import static com.anna.utils.json.Utils.jsonUtil;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fastweixin.exception.WeixinException;
import com.fastweixin.handle.MessageHandle;
import com.fastweixin.message.BaseMsg;
import com.fastweixin.message.NewsMsg;
import com.fastweixin.message.TextMsg;
import com.fastweixin.message.req.TextReqMsg;
import com.jfinal.log.Logger;
import com.tulingRobot.TulingRobotApi;
import com.tulingRobot.msg.News;
import com.tulingRobot.msg.Train;
import com.tulingRobot.msg.UrlMsg;

public class MsgDefaultHandle implements MessageHandle<TextReqMsg>{

	private static Logger log = Logger.getLogger(MsgDefaultHandle.class);

	
	@Override
	public BaseMsg handle(TextReqMsg message) {
		
		BaseMsg msg = null;
		
		try{
			String jsonStr = TulingRobotApi.process(message.getContent());
			JSONObject jsonObj = JSONObject.parseObject(jsonStr);
			int code = jsonObj.getInteger("code");
			JSONArray array = null;
			
			if(jsonObj.containsKey("list"))
				array = jsonObj.getJSONArray("list");
			
			switch (code) {
			case 100000:
				return new TextMsg(jsonObj.getString("text"));
				
			case 305000:
				NewsMsg msgT = new NewsMsg(array.size());
				Train.toMsg(msgT, array.toJSONString(),message.getContent());
				return msgT;
				
			case 200000:
				UrlMsg url = (UrlMsg)jsonUtil.json2Object(jsonStr, UrlMsg.class);
				TextMsg msgt = new TextMsg(url.getText());
				msgt.addln();
				msgt.addLink("查看新闻", url.getUrl());
				return msgt;
			case 302000:
				NewsMsg msgN = new NewsMsg(array.size());
				News.toMsg(msgN, array.toJSONString());
				return msgN;
			case 308000:
			case 306000:
				break;
			default:
				break;
			}
			
			if(code > 40000){
				throw new WeixinException("服务器错误");
			}
			
			
		}catch(Exception e){
			log.error("错误 " + message.getContent() + "\n" +e.getMessage());
		}
		
		return msg;
	}

	@Override
	public boolean beforeHandle(TextReqMsg message) {

		return true;
	}

	
	
	
}
