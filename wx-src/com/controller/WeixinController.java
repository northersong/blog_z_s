package com.controller;

import javax.servlet.http.HttpServletRequest;

import com.fastweixin.servlet.WeixinSupport;
import com.jfinal.core.Controller;
import com.jfinal.log.Logger;

public class WeixinController extends Controller {

	WeixinSupport support = new MyController();
	private static Logger log = Logger.getLogger(WeixinController.class);
	
	
	public void index(){
		HttpServletRequest request = getRequest();
        log.debug("method: " + request.getMethod());
        System.out.println("request.getMethod().toUpperCase()  :  " + request.getMethod().toUpperCase());
        //绑定微信服务器
        if ("GET".equalsIgnoreCase(request.getMethod().toUpperCase())) {
            support.bindServer(request, getResponse());
            renderNull();
        } else {
            //处理消息
            renderText(support.processRequest(request), "text/xml");
        }
	}
}
