package com.controller;

import static com.controller.service.WxService.replyService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.controller.handle.MsgDefaultHandle;
import com.controller.model.Reply;
import com.fastweixin.handle.EventHandle;
import com.fastweixin.handle.MessageHandle;
import com.fastweixin.message.BaseMsg;
import com.fastweixin.message.TextMsg;
import com.fastweixin.message.req.BaseEvent;
import com.fastweixin.message.req.BaseReqMsg;
import com.fastweixin.message.req.ImageReqMsg;
import com.fastweixin.message.req.LinkReqMsg;
import com.fastweixin.message.req.LocationEvent;
import com.fastweixin.message.req.LocationReqMsg;
import com.fastweixin.message.req.MenuEvent;
import com.fastweixin.message.req.QrCodeEvent;
import com.fastweixin.message.req.ReqType;
import com.fastweixin.message.req.ScanCodeEvent;
import com.fastweixin.message.req.SendPicsInfoEvent;
import com.fastweixin.message.req.TemplateMsgEvent;
import com.fastweixin.message.req.TextReqMsg;
import com.fastweixin.message.req.VideoReqMsg;
import com.fastweixin.message.req.VoiceReqMsg;
import com.fastweixin.servlet.WeixinSupport;
import com.jfinal.kit.PropKit;

public class MyController extends WeixinSupport {

	@Override
	protected String getToken() {
		return PropKit.get("Token", "songyangNorther");
	}
	

	/**
	 * 微信默认处理消息
	 */
	@Override
	protected List<MessageHandle> initMessageHandles() {
		
		List<MessageHandle> handles = new ArrayList<>();
		handles.add(new MsgDefaultHandle());
		
		return handles;
	}

	@Override
	protected List<EventHandle> initEventHandles() {
		return super.initEventHandles();
	}

	@Override
	protected String getAppId() {
		return PropKit.get("appId", "wx0708e2963f057121");
	}
	

	@Override
	protected String getAESKey() {
		return null;
		//return PropKit.get("AESKey", "Xms4Wfg9eUa81aPbdHvgxDwm38PMrtrCKK6LscDsY6r");
	}

	
	/**
	 * 处理文本信息
	 */
	@Override
	protected BaseMsg handleTextMsg(TextReqMsg msg) {

		String key = msg.getContent().toLowerCase();
		Reply reply = replyService.getReplyByKey(key, ReqType.TEXT);
		if (reply != null) {
			TextMsg rmsg = new TextMsg();
			rmsg.setMsgType(reply.getReplyType());
			rmsg.setContent(reply.getText());
			rmsg.setCreateTime(System.currentTimeMillis());
			
			return rmsg;
		}

		return null;
	}

	@Override
	protected BaseMsg handleImageMsg(ImageReqMsg msg) {
		// TODO Auto-generated method stub
		return super.handleImageMsg(msg);
	}

	@Override
	protected BaseMsg handleVoiceMsg(VoiceReqMsg msg) {
		// TODO Auto-generated method stub
		return super.handleVoiceMsg(msg);
	}

	@Override
	protected BaseMsg handleVideoMsg(VideoReqMsg msg) {
		// TODO Auto-generated method stub
		return super.handleVideoMsg(msg);
	}

	@Override
	protected BaseMsg handleLocationMsg(LocationReqMsg msg) {
		// TODO Auto-generated method stub
		return super.handleLocationMsg(msg);
	}

	@Override
	protected BaseMsg handleLinkMsg(LinkReqMsg msg) {
		// TODO Auto-generated method stub
		return super.handleLinkMsg(msg);
	}

	@Override
	protected BaseMsg handleQrCodeEvent(QrCodeEvent event) {
		// TODO Auto-generated method stub
		return super.handleQrCodeEvent(event);
	}

	@Override
	protected BaseMsg handleLocationEvent(LocationEvent event) {
		// TODO Auto-generated method stub
		return super.handleLocationEvent(event);
	}

	@Override
	protected BaseMsg handleMenuClickEvent(MenuEvent event) {
		// TODO Auto-generated method stub
		return super.handleMenuClickEvent(event);
	}

	@Override
	protected BaseMsg handleMenuViewEvent(MenuEvent event) {
		// TODO Auto-generated method stub
		return super.handleMenuViewEvent(event);
	}

	@Override
	protected BaseMsg handleScanCodeEvent(ScanCodeEvent event) {
		// TODO Auto-generated method stub
		return super.handleScanCodeEvent(event);
	}

	@Override
	protected BaseMsg handlePSendPicsInfoEvent(SendPicsInfoEvent event) {
		// TODO Auto-generated method stub
		return super.handlePSendPicsInfoEvent(event);
	}

	@Override
	protected BaseMsg handleTemplateMsgEvent(TemplateMsgEvent event) {
		// TODO Auto-generated method stub
		return super.handleTemplateMsgEvent(event);
	}

	@Override
	protected BaseMsg handleSubscribe(BaseEvent event) {
		// TODO Auto-generated method stub
		return super.handleSubscribe(event);
	}

	@Override
	protected BaseMsg handleUnsubscribe(BaseEvent event) {
		// TODO Auto-generated method stub
		return super.handleUnsubscribe(event);
	}

	@Override
	protected BaseMsg handleDefaultMsg(BaseReqMsg msg) {
		// TODO Auto-generated method stub
		return super.handleDefaultMsg(msg);
	}

	@Override
	protected BaseMsg handleDefaultEvent(BaseEvent event) {
		return super.handleDefaultEvent(event);
	}

	@Override
	protected boolean isLegal(HttpServletRequest request) {
		return super.isLegal(request);
	}

	
}
