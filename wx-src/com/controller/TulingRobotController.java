package com.controller;

import static com.anna.utils.json.Utils.jsonUtil;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.tulingRobot.TulingRobotApi;
import com.tulingRobot.msg.Train;

public class TulingRobotController extends Controller {

	public void index() {

		String info = getPara("info");

		String jsonStr = TulingRobotApi.process(info);
		JSONObject jsonObj = JSONObject.parseObject(jsonStr);

		JSONArray array = null;
		if (jsonObj.containsKey("list"))
			array = jsonObj.getJSONArray("list");

		Train train = null;
		StringBuffer sb = new StringBuffer();

		try {

			List<Object> list = jsonUtil.json2ObjectList(array.toJSONString(),
					Train.class);

			for (Object obj : list) {
				train = (Train) obj;
				sb.append("\n\n\n车次: " + train.getTrainnum())
						.append("\n始发站: " + train.getStart() + "-> 终点站: "
								+ train.getTerminal())
						.append("\n时间: " + train.getStarttime() + "-"
								+ train.getEndtime());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		renderText(sb.toString());
	}


}
