package com.controller.model;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Reply extends Model<Reply>{

	public final static Reply dao = new Reply();
	
	private Long id;
	private String key;
	private String msgType;
	private String replyType;
	private String text;
	
	
	public Long getId() {
		if(id == null)
			id = getLong("id");
		return id;
	}
	public void setId(Long id) {
		this.id = id;
		set("id", id);
	}
	public String getKey() {
		if(key==null)
			key = getStr("key");
		return key;
	}
	public void setKey(String key) {
		this.key = key;
		set("key", key);
	}
	public String getMsgType() {
		if(msgType == null)
			msgType = getStr("msgType");
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
		set("msgType", msgType);
	}
	public String getReplyType() {
		if(replyType == null)
			replyType = getStr("replyType");
		return replyType;
	}
	public void setReplyType(String replyType) {
		this.replyType = replyType;
		set("replyType", replyType);
	}
	public String getText() {
		if(text == null)
			text = getStr("text");
		return text;
	}
	public void setText(String text) {
		this.text = text;
		set("text", text);
	}
	
	
	
}
