package com.controller.service;

import com.controller.model.Reply;

public class ReplyService {

	public Reply getReplyByKey(String key, String msgType){
		
		return Reply.dao.findFirst("select * from reply r where r.key = ? and r.msg_type=?", key,msgType);
	}
}
