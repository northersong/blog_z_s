package com.blog.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.blog.model.Article;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class Crawler {

	private static String url1 = "http://www.oschina.net/blog/more";
	private static int maxPage = 10;
	
	
	private List<Article> articles = new ArrayList<>();
	
	private void saveArticles(){
		
		getlist(url1);
		
		if(articles.isEmpty()){
			return ;
		}
		
		for(Article a : articles){
			getArticle(a);
		}
	}
	
	private void getArticle(Article a){
		
		if(a.getOriginalUrl() == null)
			return ;
		
		try {
			Document doc = Jsoup.connect(a.getOriginalUrl()).header("Server", "Tengine/2.1.0")
					.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36")
					.get();
			a.setSummary(doc.select(".BlogAbstracts span").text());
			a.setContent(doc.select(".BlogContent").html());
			a.setAttr().save();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void getlist(String pageNum){
		try {
			
			Document doc = Jsoup.connect(url1).header("Server", "Tengine/2.1.0")
					.data("p",pageNum == null?"1":pageNum)
					.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36")
					.get();
			
			Iterator<Element> iterator = doc.select(".BlogList").select("li").iterator();
			
			Element li = null;
			Article article = null;
			while(iterator.hasNext()){
				li = iterator.next();
				article = new Article(li.select("h3").text(),li.select("h3 a").attr("href"));
				articles.add(article);
			}
			
			String nextHref = doc.select(".pager .next a").attr("href");
			int current = Integer.parseInt(doc.select(".pager .current").text());
			
			if(StrKit.notBlank(nextHref) && current < maxPage){
				getlist( ++current + "");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String...args){
		PropKit.use("a_little_config.txt");
		C3p0Plugin c3p0Plugin = new C3p0Plugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		
		ActiveRecordPlugin arp = new ActiveRecordPlugin("blog" , c3p0Plugin);
		arp.addMapping("article",Article.class);
		c3p0Plugin.start();
		arp.start();
		
		//Article article = new Article("test", "http://my.oschina.net/northerSong/blog/490194");
		//new Crawler().getArticle(article);
		new Crawler().saveArticles();
	}
}
