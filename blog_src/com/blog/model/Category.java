package com.blog.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Category extends Model<Category>{
	
	public static final Category categoryDao = new Category();
	
	public static List<Category> findList(){
		return categoryDao.find("select * from category order by sort");
	}
}
