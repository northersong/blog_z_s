package com.blog.model;


import com.jfinal.plugin.activerecord.Model;

@SuppressWarnings("serial")
public class Article extends Model<Article> {

	public static final Article articleDao = new Article();
	
	
	private String title;
	private String summary;
	private String content;
	private String type;
	private String status;
	private String originalUrl;
	public String getOriginalUrl() {
		return originalUrl;
	}
	public void setOriginalUrl(String originalUrl) {
		this.originalUrl = originalUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static Article getArticledao() {
		return articleDao;
	}
	
	
	public Article() {
		super();
	}
	
	public Article(String title,String originalUrl) {
		super();
		this.title = title;
		this.type = "crawler";
		this.status = "show";
		this.originalUrl = originalUrl;
	}
	
	public Article setAttr(){
		
		set("title", title);
		set("summary", summary);
		set("content", content);
		set("status", status);
		set("original_url",originalUrl);
		set("type",type);
		
		return this;
	}
	
	public String getTypeName(){
		Type type;
		try{
			type = Type.valueOf(getStr("type"));
		}catch(Exception e){
			e.printStackTrace();
			type = Type.unflound;
		}
		
		return type.name;
	}
	
	public enum Type{
		
		blog("博客","blog"),
		crawler("爬虫","crawler"),
		unflound("未知类型","unflound");
		
		
		private String name;
		private String val;
		
		private Type(String name, String val) {
			this.name = name;
			this.val = val;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getVal() {
			return val;
		}
		public void setVal(String val) {
			this.val = val;
		}
	}


}
