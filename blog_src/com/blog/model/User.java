package com.blog.model;

import com.jfinal.plugin.activerecord.Model;

public class User  extends Model<User>{

	private static final long serialVersionUID = 1L;
	
	public static final User userDao = new User();
	
	
	public boolean checkUserSave(){
		
		
		return true;
	}
	
	
	public enum TYPEPer{
		COMMON("普通人","common"),
		ADMIN("管理员","admin");
		
		private String name;
		private String value;
		private TYPEPer(String name, String value) {
			this.name = name;
			this.value = value;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		
		public static boolean isAdmin(String type){
			return ADMIN.getValue().equals(type);
		}
		
	}
}
