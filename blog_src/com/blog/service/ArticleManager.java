package com.blog.service;

import static com.blog.model.Article.articleDao;

import java.util.List;

import com.anna.config.Ioc.Service;
import com.blog.model.Article;
import com.jfinal.plugin.activerecord.Page;

@Service
public class ArticleManager {
	
/*	public void pri(){
		System.out.println("articleManager 的注入");
	}*/
	
	public static String Article_SQL_Head = "SELECT a.* , c.name as categoryName ";

	public static String Article_PAGESQL_BASE = "FROM article a LEFT JOIN category c ON a.categoryid = c.id"
			+ " WHERE  status = 'show'"; //a.type='blog' and
			
	public static String Article_PAGESQL_oRDER = " ORDER BY a.sort DESC, a.createtime DESC";

	
	public Page<Article> findPage(Integer pageNum){
		return articleDao.paginate(pageNum == null?1:pageNum, 3, Article_SQL_Head, Article_PAGESQL_BASE + Article_PAGESQL_oRDER);
	}
	
	public List<Article> findNear5(){
		return articleDao.find("select * from article order by createtime desc limit 5");
	}
	
	public Page<Article> findPageByMap(Integer pageNum,List<String> query){
		StringBuffer sqlExceptSelect = new StringBuffer(Article_PAGESQL_BASE);
		if(query != null){
			for(String name: query){
				sqlExceptSelect.append(" and a.").append(name);
			}
		}
		
		sqlExceptSelect.append(Article_PAGESQL_oRDER);
		return articleDao.paginate(pageNum == null?1:pageNum, 15, Article_SQL_Head,sqlExceptSelect.toString());
	}
	
/*	public static void betchSave(List<Article> list){
		
	}*/
}
