package com.blog.controller;

import com.anna.config.Ioc.Autowired;
import com.blog.model.Article;
import com.blog.model.Category;
import com.blog.service.ArticleManager;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

public class EditBlogController extends Controller{

	@Autowired
	private ArticleManager articleManager;
	
	public void editCategory(){
		
		//TODO 页面
		render("");
	}
	
	public void saveOrUpdateCategory(){
		Category category = getModel(Category.class);
		if(StrKit.notBlank(category.get("id").toString())){
			category.update();
		}else{
			category.save();
		}
		redirect("/blog/editCategory");
	}
	
	public void listArticle(){
		
		setAttr("articles", articleManager.findPageByMap(getParaToInt("pageNum", 1), null));
		
		render("/jsp/blog/manage/articlelist.jsp");
	}
	
	
	public void editArticle() throws Exception{
		// 分类
		setAttr("categorys", Category.categoryDao.find("select * from category order by sort"));
		
		Long id = getParaToLong("id");
		if(id != null){
			Article article = Article.articleDao.findById(id);
			if(article == null){
				throw new Exception("该文章已不存在");
			}
			setAttr("article",article);
		}
		render("/jsp/blog/manage/editArticle.jsp");
	}
	
	public void saveOrUpdateArticle(){
		Article article  = getModel(Article.class);
		if(article.get("id") != null ){
			article.update();
		}else{
			article.save();
		}
		redirect("/blog/editArticle");
	}
}