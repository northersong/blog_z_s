package com.blog.controller;

import java.util.ArrayList;
import java.util.List;

import com.anna.config.Ioc.Autowired;
import com.blog.model.Article;
import com.blog.model.Category;
import com.blog.service.ArticleManager;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;

public class BlogController extends Controller {

	@Autowired
	private ArticleManager articleManager;
	//= (ArticleManager)Ioc.getBean(ArticleManager.class);
	
	/**
	 * 这是博客首页
	 */
	public void index() {
		
		String search = getPara("search");
		Long categoryId = getParaToLong("categoryId");
		
		List<String> query = new ArrayList<String>();
		if(StrKit.notBlank(search))
			query.add("title like '%" + search +"%'");
		if(categoryId != null)
			query.add("categoryId =" + categoryId);
		
		// 分类
		setAttr("categorys", Category.findList());
		
		Integer pageNum = getParaToInt("pageNum");
		Page<Article> page = articleManager.findPageByMap(pageNum,query);
		setAttr("page",page);
		
		//最近的文章
		setAttr("nearArticles" ,  articleManager.findNear5());
		
		setAttr("search",search);
		setAttr("categoryId",categoryId);
		
		render("/jsp/blog/front/index.jsp");
	}
	
	
	public void article() throws Exception{
		Long id = getParaToLong("id");
		
		if(id == null && StrKit.notBlank(getAttrForStr("id")))
			id = Long.parseLong(getAttrForStr("id"));
		
		Article article = Article.articleDao.findById(id);
		
		if(article == null){
			article = Article.articleDao.findFirst(ArticleManager.Article_SQL_Head + ArticleManager.Article_PAGESQL_BASE + ArticleManager.Article_PAGESQL_oRDER);
		}
		
		setAttr("article", article);
		
		//最近的文章
		setAttr("nearArticles" ,  articleManager.findNear5());
		// 分类
		setAttr("categorys", Category.findList());

		
		render("/jsp/blog/front/single.jsp");
	}
	
	
	public void about(){
		render("/jsp/blog/front/about.jsp");
	}

}
