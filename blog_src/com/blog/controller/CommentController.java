package com.blog.controller;

import com.blog.model.Comment;
import com.blog.model.User;
import com.jfinal.core.Controller;

public class CommentController extends Controller{

	
	public void index(){
		/*String error =  getPara("error");
		String errora = getAttr("error");
		setAttr("error", getPara("error"));*/
		render("/jsp/blog/front/contact.jsp");
	}
	
	
	/**
	 * 留言给我
	 */
	public void dropToMe(){
		User user = getModel(User.class);
		
		String error = null;
		
		if(user.checkUserSave()){
			user.save();
			Comment comment = getModel(Comment.class);
			comment.set("user_id", user.get("id"))
				.set("status", "show").save();
			error = "发送成功";
		}else{
			error = "发送失败";
		}
		setAttr("error", error);
		renderText(error);
		//redirect("/contact", true);
	}
}
