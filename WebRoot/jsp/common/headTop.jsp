<%@ page language="java" contentType="text/html; charset=utf-8"%>
<div class="container">	
	<header id="site-header">
		<div class="row">
			<div class="col-md-4 col-sm-5 col-xs-8">
				<div class="logo">
					<h1><a href="/index"><b>Northern</b> &amp;songy </a></h1>
				</div>
			</div><!-- col-md-4 -->
			<div class="col-md-8 col-sm-7 col-xs-4">
				<nav class="main-nav" role="navigation">
					<div class="navbar-header">
								<button type="button" id="trigger-overlay" class="navbar-toggle">
  								<span class="ion-navicon"></span>
								</button>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<ul class="nav navbar-nav navbar-right">
  								<li class="cl-effect-11"><a href="/index" data-hover="Home">Home</a></li>
  								<li class="cl-effect-11"><a href="/article" data-hover="Article">Article</a></li>
  								<li class="cl-effect-11"><a href="/lab" data-hover="Lab">Lab</a></li>
  								<li class="cl-effect-11"><a href="/about" data-hover="About">About</a></li>
  								<li class="cl-effect-11"><a href="/contact" data-hover="Contact">Contact</a></li>
								</ul>
					</div><!-- /.navbar-collapse -->
				</nav>
				<div id="header-search-box">
					<a id="search-menu" href="#"><span id="search-icon" class="ion-ios-search-strong"></span></a>
					<div id="search-form" class="search-form">
						<form role="search" method="post" id="searchform" action="/">
							<input type="search" placeholder="Search" name="search" required>
							<button type="submit"><span class="ion-ios-search-strong"></span></button>
						</form>		
					</div>
				</div>
			</div><!-- col-md-8 -->
		</div>
	</header>
</div>