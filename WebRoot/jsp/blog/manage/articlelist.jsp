<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>后台管理</title>
 <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet">
 <script type="text/javascript" src="/js/jquery.js"></script>
 <script type="text/javascript" charset="utf-8" src="/Plugin/uedit/ueditor.config.js"></script>
 <script type="text/javascript" charset="utf-8" src="/Plugin/uedit/ueditor.all.min.js"> </script>
 <script type="text/javascript" charset="utf-8" src="/Plugin/uedit/lang/zh-cn/zh-cn.js"></script>
</head>
<body>
<div class="container">
	<div class="page-header">
		<h1>Hello world <small>_anna</small></h1>
	</div>
	<div class="row">
	<jsp:include page="/jsp/blog/manage/leftNav.jsp" flush="false"/>
	<div class="col-xs-10">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>#</th><th>标题</th><th>分类</th><th>类型</th><th>时间</th><th>操作</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${articles.list}" var="article" varStatus="index">
				<tr>
					<td>${index.index + 1 }</td>
					<td>${article.title }</td>
					<td>${article.categroyName }</td>
					<td>${article.getTypeName() }</td>
					<td>${article.createtime }</td>
					<td>
					<a href="/blog/editArticle?id=${article.id }">编辑</a> |
					<a href="/article/${article.id }.htm">查看</a>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>	
	</div>
	</div>
</div>
</body>
</html>
