<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>后台管理</title>
 <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet">
 <script type="text/javascript" src="/js/jquery-2.1.3.min.js"></script>
 <script type="text/javascript" charset="utf-8" src="/Plugin/uedit/ueditor.config.js"></script>
 <script type="text/javascript" charset="utf-8" src="/Plugin/uedit/ueditor.all.min.js"> </script>
 <script type="text/javascript" charset="utf-8" src="/Plugin/uedit/lang/zh-cn/zh-cn.js"></script>
</head>

<div class="container">
	<div class="page-header">
		<h1>Hello world <small>_anna</small></h1>
	</div>
	<div class="row">
	<jsp:include page="/jsp/blog/manage/leftNav.jsp" flush="false"/>
	<div class="col-xs-10">
		<h3>文章</h3>
		<form id="form" action="/blog/saveOrUpdateArticle" method="post">
		  <div class="form-group">
			<label>博客标题</label>
			<input type="text" class="form-control" placeholder="标题" name="article.title" value="${article.title }">
		  </div>
		  <div class="form-group">
		  	<label>博客摘要</label>
			<textarea class="form-control" rows="3" name="article.summary" >${article.summary }</textarea>
		  </div>
		  <div class="form-group">
			<label for="exampleInputPassword1">博客内容</label>
			<script id="editor" type="text/plain" style="width:100%;height:500px;"></script>
			<textarea style="display:none" class="form-control" rows="3" id="content" name="article.content">${article.content }</textarea>
		  </div>
		  <button type="submit" class="btn btn-default" >Submit</button>
		</form>
	</div>
	</div>
	<hr>
	<div class="row">
	
		<div class="col-xs-4">
		</div>
		<div class="col-xs-4">
		<h5>轻抚绣冬刀</h5>
		</div>
		<div class="col-xs-4">
		</div>
	</div>
</div>
<script type="text/javascript">
var ue = UE.getEditor("editor");
var text = $("#content").val();

if(!!text){
	ue.ready(function(editorObj){
		ue.setContent(text);
	});
}
$("#form").submit(function(){
	$("#content").val(ue.getContent());
});
</script>
<body>
</body>
</html>