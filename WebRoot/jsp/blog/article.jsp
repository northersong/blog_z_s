<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${article.title }</title>
 <link href="/css/bootstrap/bootstrap.min.css" rel="stylesheet">
 <link href="/css/blog.css" rel="stylesheet">
</head>
<body>
<div class="container">
	<h1>Hello world <small>kk_anna</small></h1>
	<ul class="nav nav-tabs">
	  <li role="presentation" ><a href="#">首页</a></li>
	  <li role="presentation" class="active"><a href="#">博文</a></li>
	  <li role="presentation"><a href="#">Messages</a></li>
	</ul>
	<div class="row">
		<div class="col-sm-8 blog-main">
			<div class="blog-post">
				<h2 class="blog-post-title">${article.title }</h2>
				<p class="blog-post-meta">${article.createtime } by <a href="#">Mark</a></p>
					${article.content}
			</div>
		</div>
  	</div>
</div>
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>