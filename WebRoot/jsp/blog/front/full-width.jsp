<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>Black &amp; White</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/staticFiles.jsp" %>
</head>
<body>
<jsp:include page="/jsp/common/headTop.jsp" />
<div class="content-body">
	<div class="container">
		<div class="row">
			<main class="col-md-12">
				<c:forEach items="${page.list }" var="article" varStatus="index"> 
				<article class="post post-${index.index+1 }">
					<header class="entry-header">
						<h1 class="entry-title">
							<!--文章标题  -->
							<a href="/article/${article.id }.htm">${article.title }</a>
						</h1>
						<!--文章meta  -->
						<div class="entry-meta">
							<span class="post-category"><a href="#">${article.categoryName }</a></span>
							<span class="post-date"><a href="#"><time class="entry-date"  datetime="${article.createtime }">
							<fmt:formatDate value="${article.createtime }" type="both"/> </time></a></span>
							<!-- <span class="post-author"><a href="#"></a></span> -->
							<span class="comments-link"><a href="#">Northernsongy</a></span>
						</div>
					</header>
					<!--摘要  -->
					<div class="entry-content clearfix">
						<p>${article.summary }</p>
						<!--继续阅读  -->
						<div class="read-more cl-effect-14">
							<a href="/article/${article.id }.htm" class="more-link">Continue reading <span class="meta-nav">→</span></a>
						</div>
					</div>
				</article>
				</c:forEach>
			</main>
		</div>
	</div>
</div>
<jsp:include page="/jsp/common/foot.jsp" />
</body>
</html>