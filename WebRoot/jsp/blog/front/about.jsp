<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>Black &amp; White</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/staticFiles.jsp" %>
</head>
<body id="page">
<jsp:include page="/jsp/common/headTop.jsp" />
<div class="content-body">
	<div class="container">
		<div class="row">
			<main class="col-md-12">
				<h1 class="page-title">About Me</h1>
				<article class="post">
					<div class="entry-content clearfix">
						<figure class="img-responsive-center">
							<img class="img-responsive" src="/img/me.jpg" alt="Developer Image">
						</figure>
						<p>我只是不想用我的电脑玩游戏，然后就只在电脑上装了ubuntu。</p>
						<p>然后我不想让我的电脑闲着，然后就用ubuntu装各种工具。</p>
						<p>然后就有了这个网站</p>
						<div class="height-40px"></div>
						<h2 class="title text-center">Social</h2>
						<ul class="social">
							<li class="facebook"><a href="#"><span class="ion-social-facebook"></span></a></li>
							<li class="twitter"><a href="#"><span class="ion-social-twitter"></span></a></li>
							<li class="google-plus"><a href="#"><span class="ion-social-googleplus"></span></a></li>
							<li class="tumblr"><a href="#"><span class="ion-social-tumblr"></span></a></li>
						</ul>
					</div>
				</article>
			</main>
		</div>
	</div>
</div>
<jsp:include page="/jsp/common/foot.jsp" />
</body>
</html>