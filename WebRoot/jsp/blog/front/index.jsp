<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>Black &amp; White</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/staticFiles.jsp" %>
</head>
<body>
<jsp:include page="/jsp/common/headTop.jsp" />
<div class="content-body">
	<div class="container">
		<div class="row">
			<main class="col-md-8">
			<c:forEach items="${page.list }" var="article" varStatus="index"> 
			<article class="post post-${index.index+1 }">
					<header class="entry-header">
						<h1 class="entry-title">
							<!--文章标题  -->
							<a href="/article/${article.id }.htm">${article.title }</a>
						</h1>
						<!--文章meta  -->
						<div class="entry-meta">
							<span class="post-category"><a href="#">${article.categoryName }</a></span>
							<span class="post-date"><a href="#"><time class="entry-date"  datetime="${article.createtime }">
							<fmt:formatDate value="${article.createtime }" type="both"/> </time></a></span>
							<!-- <span class="post-author"><a href="#"></a></span> -->
							<span class="comments-link"><a href="#">Northernsongy</a></span>
						</div>
					</header>
					<!--摘要  -->
					<div class="entry-content clearfix">
						<p>${article.summary }</p>
						<!--继续阅读  -->
						<div class="read-more cl-effect-14">
							<a href="/article/${article.id }.htm" class="more-link">Continue reading <span class="meta-nav">→</span></a>
						</div>
					</div>
				</article>
			</c:forEach>
			<article>
			
			<!-- 分页 -->
			<div id="pageDiv">
			<header class="entry-header">
			<nav style="margin-left: 45%">
			  <ul class="pagination">
			  </ul>
			</nav>
			</header>
			</div>
			
			</article>
			</main>
			
			<!--右侧导航  -->
			<aside class="col-md-4">
				<!--最近的帖子  -->
				<div class="widget widget-recent-posts">		
					<h3 class="widget-title">Recent Posts</h3>		
					<ul>
						<c:forEach items="${nearArticles }" var="article">
						<li>
							<a href="/article/${article.id }.htm">${article.title }</a>
						</li>
						</c:forEach>
					</ul>
				</div>
				<!--归档  -->
				<!-- <div class="widget widget-archives">		
					<h3 class="widget-title">Archives</h3>		
					<ul>
						<li>
							<a href="#">November 2014</a>
						</li>
						<li>
							<a href="#">September 2014</a>
						</li>
						<li>
							<a href="#">January 2013</a>
						</li>
					</ul>
				</div> -->
				<!--分类  -->
				<div class="widget widget-category">		
					<h3 class="widget-title">
					<a href="/">Category</a></h3>
					<ul>
					<c:forEach items="${categorys }" var="category">
					   <li>
						   	<span class="${category.id == categoryId?'ion-ios-checkmark':'ion-ios-checkmark-outline'} "></span>
							<a href="/?categoryId=${category.id }">${category.name }</a>
						</li>
					</c:forEach>
					</ul>
				</div>
			</aside>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	initPage();
});
function initPage(){
	if(${page.totalPage} < 2){
		return false;
	}
	
	$.jqPaginator('.pagination',{
		totalCounts:parseInt(${page.totalRow}),
		pageSize:parseInt(${page.pageSize}),
	    visiblePages: 10,
	    currentPage: parseInt(${page.pageNumber}),
	    activeClass:'active',
	    prev:'<li><a href="javascript:void(0)" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>',
	    next:'<li><a href="javascript:void(0)" aria-label="next"><span aria-hidden="true">&raquo;</span></a></li>',
	    page:'<li><a href="javascript:void(0)">{{page}}</a></li>',
	    onPageChange: function (num, type) {
	    	if(type == 'init'){
	    	//	$(".multiTotal").html('<em class="allPage" id="totalNum">共'+ parseInt(pageInfo.total) +'条</em><span id="totalPage" style="display:none;">' + pageInfo.totalPage + '</span>');
	    		return false;
	    	}
	    	turnPage(num);
	    }
	});
}
function turnPage(num){
	$("#pageNum").val(num);
	$("#pageNum").parent("form").ation = window.location.href;
	$("#pageNum").parent("form").submit();
}
</script>
<form >
	<input id="pageNum" name="pageNum" type="hidden">
</form>
<jsp:include page="/jsp/common/foot.jsp" />
<script src="/js/jqPaginator/jqPaginator.js"></script>
</body>
</html>