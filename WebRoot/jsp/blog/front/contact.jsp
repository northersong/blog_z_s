<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>Black &amp; White</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/staticFiles.jsp" %>
</head>
<body id="page">
<jsp:include page="/jsp/common/headTop.jsp" />
<div class="content-body">
	<div class="container">
		<div class="row">
			<main class="col-md-12">
				<h1 class="page-title">Contact</h1>
				<article class="post">
					<div class="entry-content clearfix">
						<form action="/contact/dropToMe" onsubmit="javascript:return false;" method="post" class="contact-form">
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<input type="text" name="user.name" placeholder="Name" required>
									<input type="email" name="user.email" placeholder="Email" required>
									<input type="text" name="comment.subject" placeholder="Subject" required>
									<textarea name="comment.comment" rows="7" placeholder="Your Message" required></textarea>
									<button id="drop" class="btn-send btn-5 btn-5b ion-ios-paperplane"><span>Drop Me a Line</span></button>
								</div>
							</div>	<!-- row -->
						</form>
					</div>
				</article>
			</main>
		</div>
	</div>
</div>
<jsp:include page="/jsp/common/foot.jsp" />
</body>
<script type="text/javascript">
$("#drop").click(function(){
	$.post("/contact/dropToMe",
			$("form").serialize(),
			function(text){
				alert(text);	
				if(text == "发送成功"){
					$("from").find("input").each(function(){
						$(this).val("");
					});
				}
		});
});
</script>
</html>