<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE html>
<html>
<head>
<title>Black &amp; White</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ include file="/common/staticFiles.jsp" %>
</head>
<body id="page">
<jsp:include page="/jsp/common/headTop.jsp" />
<div class="content-body">
	<div class="container">
		<div class="row">
			<main class="col-md-8">
				<article class="post post-1">
					<header class="entry-header">
						<h1 class="entry-title">${article.title }</h1>
						<div class="entry-meta">
							<!-- <span class="post-category"><a href="#">Web Design</a></span> -->
							<span class="post-date"><a href="#"><time class="entry-date" datetime="${article.createtime }">
							<fmt:formatDate value="${article.createtime }" type="both"/> </time></a></span>
							<span class="post-author"><a href="#">Northernsongy</a></span>
							<span class="comments-link"><a href="#">4 Comments</a></span>
						</div>
					</header>
					<div class="entry-content clearfix">
						${article.content }
					</div>
				</article>
			</main>
			<aside class="col-md-4">
				<div class="widget widget-recent-posts">		
					<h3 class="widget-title">Recent Posts</h3>		
					<ul>
						<c:forEach items="${nearArticles }" var="article">
						<li>
							<a href="/article/${article.id }.htm">${article.title }</a>
						</li>
						</c:forEach>
					</ul>
				</div>
				<!-- <div class="widget widget-archives">		
					<h3 class="widget-title">Archives</h3>		
					<ul>
						<li>
							<a href="#">November 2014</a>
						</li>
						<li>
							<a href="#">September 2014</a>
						</li>
						<li>
							<a href="#">January 2013</a>
						</li>
					</ul>
				</div> -->

				<div class="widget widget-category">
					<h3 class="widget-title">Category</h3>
					<ul>
						<c:forEach items="${categorys }" var="category">
					   <li>
							<a href="#">${category.name }</a>
						</li>
					</c:forEach>
					</ul>
				</div>
			</aside>
		</div>
	</div>
</div>
<jsp:include page="/jsp/common/foot.jsp" />
</body>
</html>