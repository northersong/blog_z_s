<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@include file="/common/tagHead.jsp" %>
<!DOCTYPE HTML>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta HTTP-EQUIV="Expires" CONTENT="-1">
<title>北风轻抚绣冬刀</title>
<link rel="stylesheet" type="text/css" href="/css/global.css" />
<script type="text/javascript" src="/js/jquery.js"></script>
</head>
<body>
	<div class="start">
		<div class="content">
			<h1 class="name">NorthernSongyang</h1>
			<div class="info">Hello holld</div>
			<c:forEach items="${categorys }" var="c">
			<div class="nav"><a href="/categroys/${c.id }">${c.name}</a></div>
			</c:forEach>
		</div>
		<div class="fl mainRight">
			<div class="myRelease">
				<ul>
					<c:forEach items="${articles}" var="article">
						<li><a href="/article/${article.id }.htm">${article.title }</a></li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</div>
	<script>
	$(".mainRight").mouseover(function () {
        $(".myRelease").show();
    })
    $(".mainRight").mouseout(function () {
        $(".myRelease").hide();
    })
	</script>
</body>
</html>