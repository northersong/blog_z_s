<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
	//该文件只存放jstl等相关文件，不允许添加静态文件如script等，静态文件使用staticFiles.jsp
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName();
	if(request.getServerPort() != 80)
	{
		basePath += ":" + request.getServerPort() + path;
	}
	basePath += "/";
	String uri = request.getRequestURI();
%>
<c:set var="path" value="<%=path%>" />
<c:set var="basePath" value="<%=basePath%>" />
<c:set var="uri" value="<%=uri %>"/>
