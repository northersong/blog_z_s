<%@ page language="java" contentType="text/html; charset=utf-8"%>
<!-- css -->
<link href="/img/favicon.ico" rel="shortcut icon"/>
<link rel="stylesheet" href="/css/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="/css/bootstrap/ionicons.min.css">
<link rel="stylesheet" href="/css/pace.css">
<link rel="stylesheet" href="/css/custom.css">

<!-- js -->
<script src="/js/jquery-2.1.3.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/pace.min.js"></script>
<script src="/js/modernizr.custom.js"></script>
